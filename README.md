# UnitConversion

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Future Enhancement

* Provide decimal selection option to check the accurate answer.
* In the same form provide swap button for to and from units. So that if teacher selected wrong unit then they can simply swap it.
* For easy entry we can give short cuts so that it will be easier for teachers to enter data e.g. F2C, C2K, K2F
* Intergrate with google sheet so teachers can enter all data in google sheet and we can fetch it from there. Or make the form in tabular format.
* Add one more field for student id. So that we can keep track of student performance.
* Once we have student id so that we can generate more reports by student.
* Redesign the panel to allow teachers to create test from the same place.
* After that allow student to signup and give the test from here only so there is no need for teacher to add result. And from there we can have all the required details to generate all the reports from here.
* Once student done with the test they can have access to other part of the app as well.
    * We will give radio button to select measure if we have only few measures. E.g. Temperature and Volume. Or we have to give dropdown for all available measures.
    * Once they select measure we will display all available units related to that measure. And from there student can enter input and select the input unit after that we will display conversion to all related units.
    * While conversion provide details tab to view the convesation formula. 
* Add web accessibility standards to the website so that everyone can access the website easily.