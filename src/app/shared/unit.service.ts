import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  constructor() { }

  form: FormGroup = new FormGroup({
    inputValue: new FormControl('', Validators.required),
    inputUnitMeasure: new FormControl('', Validators.required),
    targetUnitMeasure: new FormControl('', Validators.required),
    studentResponse: new FormControl('', Validators.required),
  });

  initializeFormGroup() {
    this.form.setValue({
      inputValue: '',
      inputUnitMeasure: '',
      targetUnitMeasure: '',
      studentResponse: '',
    });
  }
}
