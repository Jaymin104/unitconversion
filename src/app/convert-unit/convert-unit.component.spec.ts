import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ConvertUnitComponent } from './convert-unit.component';

describe('ConvertUnitComponent', () => {
  let component: ConvertUnitComponent;
  let fixture: ComponentFixture<ConvertUnitComponent>;
  let convert = require('convert-units');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvertUnitComponent ],
      imports: [
        BrowserModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatTableModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvertUnitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should convert from C to K', () => {
    let actual = convert(0).from('C').to('K');
    let expected = 273.15;
    expect(actual).toBe(expected);
  });

  it('should throw exception while converting from C to l', () => {
    let exception = "Cannot convert incompatible measures of volume and temperature";
    expect(() => { convert(0).from('C').to('l'); }).toThrowError(exception);
  });

  it('should store data for correct result', () => {
    // const convertSpy = jasmine.createSpyObj<ConvertUnitComponent>('ConvertUnitComponent', ['convert']);
    // const storeDataSpy = jasmine.createSpyObj<ConvertUnitComponent>('ConvertUnitComponent', ['storeData']);
    // storeDataSpy.storeData.and.callThrough();
    // spyOn(component, 'convert').and.returnValue(1);
    // spyOn(component, 'storeData').and.callThrough();
    // component.onSubmit();
    // expect(component.storeData).toHaveBeenCalled();
  });
});
