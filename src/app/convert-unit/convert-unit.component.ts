import { Component, OnInit } from '@angular/core';
import { UnitService } from '../shared/unit.service';

@Component({
  selector: 'app-convert-unit',
  templateUrl: './convert-unit.component.html',
  styleUrls: ['./convert-unit.component.css']
})
export class ConvertUnitComponent implements OnInit {

  convert = require('convert-units');
  displayedColumns: string[] = ['inputNumericalValue', 'inputUnitofMeasure', 'targetUnitofMeasure', 'studentResponse', 'output', 'correctAnswer'];
  inputUnitIndex: number; //To store the index of input unit of measure array
  targetUnitIndex: number; //To store the index of target unit
  temperatureUnit: any; //To store the temperature units in an array
  volumeUnit: any; //To store the volume units in an array
  invalidUnit: any = [{ abbr: 'dog', plural: 'dog' }]; //This is dummy array just to see invalid
  allUnit: any; //Store all units in single array
  dataSource: any = []; // Store user's input data

  constructor(public service: UnitService) { }

  ngOnInit() {
    this.temperatureUnit = this.convert().list('temperature'); //Get all temperature units and store the result in an array
    this.volumeUnit = this.convert().list('volume'); ////Get all volume units and store the result in an array
    this.allUnit = [...this.temperatureUnit, ...this.volumeUnit, ...this.invalidUnit]; //Merge all 3 arrays
  }

  //To clear the form data
  onClear() {
    this.service.form.reset();
    // this.service.initializeFormGroup();
  }

  //Submit Data
  onSubmit() {
    let inputArray = this.service.form.value; //Store user inputed data in inputarray
    this.inputUnitIndex = inputArray.inputUnitMeasure;
    this.targetUnitIndex = inputArray.targetUnitMeasure;
    try {
      let answer = (Math.round(this.convert(inputArray.inputValue).from(this.allUnit[this.inputUnitIndex].abbr).to(this.allUnit[this.targetUnitIndex].abbr)* 10) / 10);
      if(answer === (Math.round(inputArray.studentResponse * 10) / 10)) {
        this.storeData(inputArray, answer, 'Correct');
      } else {
        this.storeData(inputArray, answer, 'Incorrect');
      }
    } catch(error) {
      this.storeData(inputArray, 'Invalid', 'Invalid');
    }
    this.onClear();
  }

  //Store dataset in datasource array
  storeData(inputArray, correctValue, result) {
    let tempArray: any = {};
    tempArray.inputNumericalValue = inputArray.inputValue;
    tempArray.inputUnitofMeasure = this.allUnit[this.inputUnitIndex].plural;
    tempArray.targetUnitofMeasure = this.allUnit[this.targetUnitIndex].plural;
    tempArray.studentResponse = inputArray.studentResponse;
    tempArray.output = result;
    tempArray.correctAnswer = correctValue;
    this.dataSource = [...this.dataSource, tempArray];
  }

}
